       IDENTIFICATION DIVISION.
       PROGRAM-ID. BMI.
       AUTHOR. GlauyHom(mind).
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 Weight       PIC 9(3).
       01 Height       PIC 9(3).
       01 BMI          PIC 9(4)V9(2).

       PROCEDURE DIVISION.
           DISPLAY "Please enter your weight (in kilograms): ".
           ACCEPT Weight.
       
           DISPLAY "Please enter your height (in meters): ".
           ACCEPT Height.
       
           COMPUTE BMI = Weight / (Height * Height).
       
           DISPLAY "Your BMI is: " BMI.
       
           IF BMI < 18.5
               DISPLAY "You are underweight."     
           ELSE IF BMI >= 18.5 AND BMI < 24.9
               DISPLAY "You are normal weight."
           ELSE IF BMI >= 25.0 AND BMI < 29.9
               DISPLAY "You are overweight."
           ELSE
               DISPLAY "You are obese."
           END-IF.
       
           STOP RUN.
       